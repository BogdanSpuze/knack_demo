import { expect, Locator, Page } from '@playwright/test';

export class LiveMainPage {
  readonly page: Page;
  readonly pageUrl: string;

  readonly inventory: Locator;
  readonly addFilters: Locator;
  
  recordIdentifier: Locator;
  
  public constructor(page: Page) {
    this.page = page;
    this.pageUrl = 'https://toolker.knack.com/toolker-warehouse-manager#home/'; //TODO: should be taken from config file

    this.inventory = this.page.locator('a', { hasText: 'Inventory' }).nth(0); //be carefull with translations - find another way to identify this button
    this.addFilters = this.page.locator('.kn-add-filter');
  }

  public async goto() {
    await this.page.goto(this.pageUrl);
  }

  public async clickInventory() {
    await this.inventory.click();
  }

  public async clickAddFilters() {
    await this.addFilters.click();
  }

  public async getNumberOfRecords(){
    const knLoader = await this.page.locator('#kn-loader');
    await knLoader.waitFor({state: 'hidden'});

    await this.page.waitForTimeout(5000); // todo: Improve this wait, this is not good, but ok for now

    this.recordIdentifier = await this.page.locator('.field_142');
    return await this.recordIdentifier.count()-1; // it counts the header also
  }


  public async getNumberOfNeedsReorderYes(){
    this.recordIdentifier = await this.page.locator('.field_142');
    var i = 0;
    for (const li of await this.recordIdentifier.all())
      if((await li.innerText()).includes('Yes'))
        i++;
    return await i;
  }

}