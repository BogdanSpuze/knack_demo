import { expect, Locator, Page } from '@playwright/test';

export class LiveLoginPage {
  readonly page: Page;
  readonly pageUrl: string;

  readonly emailField: Locator;
  readonly passwordField: Locator;
  readonly signInButton: Locator;

  public constructor(page: Page) {
    this.page = page;
    this.pageUrl = 'https://toolker.knack.com/toolker-warehouse-manager#home/';   //TODO: this should be taken from config file


    this.emailField = this.page.locator('input#email');
    this.passwordField = this.page.locator('input#password');
    this.signInButton = this.page.locator('input', { hasText: 'Sign In' }); //be carefull with translations - find another way to identify this button
    
  }

  public async goto() {
    await this.page.goto(this.pageUrl);
  }

  public async login(emailStr: string, passStr: string) {
    await this.emailField.type(emailStr);
    await this.passwordField.type(passStr);
    await this.signInButton.click();
  }

}