import { expect, Locator, Page } from '@playwright/test';

export class BuilderMainPage {
  readonly page: Page;
  readonly pageUrl: string;

  readonly topBar: Locator;
  readonly recordsIcon: Locator;
  readonly warehouseInventory: Locator;
  readonly addFilters: Locator;
  editRecordButton: Locator;
  needsReorderCell: Locator;
  
  public constructor(page: Page) {
    this.page = page;
    this.pageUrl = 'https://builder.knack.com/toolker/toolker-warehouse-manager/schema/list'; //TODO: this should be taken from config file


    this.topBar = this.page.locator('#topbar');
    this.recordsIcon = this.page.locator('.icon-database-records');
    this.warehouseInventory = this.page.locator('[data-cy="Object Warehouse Inventory"]'); //TODO: be carefull when transaltions are going to come
    this.addFilters = this.page.locator('[data-cy=add-filters]');
  }

  public async goto() {
    await this.page.goto(this.pageUrl);
  }

  public async getTopBarText() {
    return await this.topBar.textContent();
  }

  public async clickRecords() {
    await this.recordsIcon.click();
  }

  public async clickWarhouseInventory() {
    await this.warehouseInventory.click();
  }

  public async clickAddFilters() {
    await this.addFilters.click();
  }

  public async getNumberOfRecords(){
    const knLoader = await this.page.locator('#kn-loader');
    await knLoader.waitFor({state: 'hidden'});

    await this.page.waitForTimeout(5000); // todo: Improve this wait, this is not good, but ok for now

    this.editRecordButton = await this.page.locator('[data-cy=table-cell-field_142]');
    return await this.editRecordButton.count();
  }

  public async getNumberOfNeedsReorderYes(){
    this.needsReorderCell = await this.page.locator('[data-cy=table-cell-field_142]');
    var i = 0;
    for (const li of await this.needsReorderCell.all())
      if((await li.innerText()).includes('Yes'))
        i++;
    return await i;
  }
}