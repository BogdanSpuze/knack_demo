import { expect, Locator, Page } from '@playwright/test';

export class BuilderLoginPage {
  readonly page: Page;
  readonly pageUrl: string;

  readonly emailField: Locator;
  readonly passwordField: Locator;
  readonly signInButton: Locator;

  public constructor(page: Page) {
    this.page = page;
    this.pageUrl = 'https://builder.knack.com/toolker/toolker-warehouse-manager/login'; 


    this.emailField = page.locator('input#email').nth(1);
    this.passwordField = page.locator('input#password').nth(1);
    this.signInButton = page.locator('input', { hasText: 'Sign In' }).nth(1); //be carefull with translations - find another way to identify this button
    
  }

  public async goto() {
    await this.page.goto(this.pageUrl);
  }

  public async login(emailStr: string, passStr: string) {
    await this.emailField.type(emailStr);
    await this.passwordField.type(passStr);
    await this.signInButton.click();
  }

}