import { Page } from '@playwright/test';
import { FiltersBase } from '../FiltersBase';

export class BuilderFilters extends FiltersBase{
  public constructor(page: Page) {
    super(page);
  
    this.fieldDropDown = this.page.locator('.field-list-field');
    this.operatorDropDown = this.page.locator('.field-list-operator');
    this.submitButton = this.page.locator('[data-cy=save-filters]');
  }

  public async initValueDropDownYesNo(){
      this.valueDropDown = this.page.locator('[data-cy=dropdown-select]')
  }
}