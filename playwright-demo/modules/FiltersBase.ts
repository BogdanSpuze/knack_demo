import { expect, Locator, Page } from '@playwright/test';


export abstract class FiltersBase{
  page: Page;

  fieldDropDown: Locator;
  operatorDropDown: Locator;
  valueDropDown: Locator;
  submitButton: Locator;
  
  public constructor(page: Page) {
    this.page = page;
  }

  public abstract initValueDropDownYesNo();

  public async setFieldOption(value: string){
      this.fieldDropDown.selectOption({label: value});
  }

  public async setOperatorOption(value: string){
    this.operatorDropDown.selectOption({label: value});
  }

  public async setValueOption(value: string){
    this.valueDropDown.selectOption({label: value});
  }

  public async clickSubmit(){
      this.submitButton.click();
  }

  public async filterBy(field: string, operator: string, value: string){
    await this.setFieldOption(field);
    await this.setOperatorOption(operator);
    await this.initValueDropDownYesNo();
    await this.setValueOption(value);
    await this.clickSubmit();
  }
}