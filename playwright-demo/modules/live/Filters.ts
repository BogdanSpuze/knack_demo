import { Page } from '@playwright/test';
import { FiltersBase } from '../FiltersBase';

export class LiveFilters extends FiltersBase {
  public constructor(page: Page) {
    super(page);

    this.fieldDropDown = this.page.locator('[name=field]').nth(1);
    this.operatorDropDown = this.page.locator('[name=operator]').nth(1);
    this.submitButton = this.page.locator('#kn-submit-filters');
  }

  public async initValueDropDownYesNo(){
    this.valueDropDown = this.page.locator('[name=value]').nth(1);
  }
}