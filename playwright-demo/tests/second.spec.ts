import { test, expect } from '@playwright/test';
import { BuilderLoginPage } from '../pages/builder/login';
import { BuilderMainPage } from '../pages/builder/main';
import { BuilderFilters } from '../modules/builder/Filters';
import { LiveFilters } from '../modules/live/Filters';
import { LiveLoginPage } from '../pages/live/login';
import { LiveMainPage } from '../pages/live/main';




test('warehouse manager count after filter test', async ({ page }) => {
  const builderLogin = new BuilderLoginPage(page);
  const builderMain = new BuilderMainPage(page);
  const builderFilters = new BuilderFilters(page);
  const liveFilters = new LiveFilters(page);
  const liveLoginPage = new LiveLoginPage(page);
  const liveMainPage = new LiveMainPage(page);

  await builderLogin.goto();
  await builderLogin.login('xxxxx@toolker.com', 'xxxxx'); //TODO: take the login from configuration file
  expect((await builderMain.getTopBarText()).toLowerCase()).toContain('toolker warehouse manager'); //TODO: take the name from the configuration file

  await builderMain.clickRecords();
  await builderMain.clickWarhouseInventory();
  await builderMain.clickAddFilters();

  await builderFilters.filterBy('Needs Re-Order', 'is', 'Yes');  //TODO: be carefull when translations are going to happen!


  var numberOfRecords = await builderMain.getNumberOfRecords();
  var needsReorder = await builderMain.getNumberOfNeedsReorderYes();

  expect(numberOfRecords).toBe(needsReorder);
  

  await liveLoginPage.goto();
  await liveLoginPage.login('admin@test.com', 'test'); //TODO: take the login from configuration file
  await liveMainPage.clickInventory();
  await liveMainPage.clickAddFilters();
  
  await liveFilters.filterBy('Needs Re-Order', 'is', 'Yes');  //TODO: be carefull when translations are going to happen!

  var liveNumberOfRecords = await liveMainPage.getNumberOfRecords();
  var liveNeedsReorder = await liveMainPage.getNumberOfNeedsReorderYes();

  expect(liveNumberOfRecords).toBe(liveNeedsReorder);
  expect(numberOfRecords).toBe(liveNumberOfRecords);
});

